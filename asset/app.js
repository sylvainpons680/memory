const cardsArray = [{
	'name': 'shell',
	'img': 'https://image.noelshack.com/fichiers/2019/10/4/1551950131-blueshell.png',
},
{
	'name': 'star',
	'img': 'https://image.noelshack.com/fichiers/2019/10/4/1551950591-star.png',
},
{
	'name': 'bobomb',
	'img': 'https://image.noelshack.com/fichiers/2019/10/4/1551950617-bobomb.png',
},
{
	'name': 'mario',
	'img': 'https://image.noelshack.com/fichiers/2019/10/4/1551950642-mario.png',
},
{
	'name': 'luigi',
	'img': 'https://image.noelshack.com/fichiers/2019/10/4/1551950663-luigi.png',
},
{
	'name': 'peach',
	'img': 'https://image.noelshack.com/fichiers/2019/10/4/1551950683-peach.png',
},
{
	'name': '1up',
	'img': 'https://raw.githubusercontent.com/taniarascia/memory/master/img/1up.png',
},
{
	'name': 'mushroom',
	'img': 'https://image.noelshack.com/fichiers/2019/10/4/1551950795-mushroom.png',
},
{
	'name': 'thwomp',
	'img': 'https://image.noelshack.com/fichiers/2019/10/4/1551950815-thwomp.png',
},
{
	'name': 'bulletbill',
	'img': 'https://image.noelshack.com/fichiers/2019/10/4/1551950830-bulletbill.png',
},
{
	'name': 'coin',
	'img': 'https://image.noelshack.com/fichiers/2019/10/4/1551950848-coin.png',
},
{
	'name': 'goomba',
	'img': 'https://image.noelshack.com/fichiers/2019/10/4/1551950869-goomba.png',
},
];

const gameGrid = cardsArray
//doublé le nombre de card
  .concat(cardsArray)
  // randomiser les cards
  .sort(() => 0.5 - Math.random());

let firstGuess = '';
let secondGuess = '';
let count = 0;
let previousTarget = null;
let delay = 1200;

const game = document.getElementById('game');
//creation d'une section
const grid = document.createElement('section');
//creation d'une classe grid dans section
grid.setAttribute('class', 'grid');
game.appendChild(grid);


gameGrid.forEach(item => {
  const { name, img } = item;
//creation de fiv
  const card = document.createElement('div');
  //application d'une class a la div
  card.classList.add('card');
  // Set the data-name attribute of the div to the cardsArray name
  card.dataset.name = name;

  const front = document.createElement('div');
  front.classList.add('front');

  const back = document.createElement('div');
  back.classList.add('back');
  //applique l'imae sur la div 
  back.style.backgroundImage = `url(${img})`;

  grid.appendChild(card);
  card.appendChild(front);
  card.appendChild(back);
});

const match = () => {
  const selected = document.querySelectorAll('.selected');
  selected.forEach(card => {
    card.classList.add('match');
  });
};

const resetGuesses = () => {
  firstGuess = '';
  secondGuess = '';
  count = 0;
  previousTarget = null;

  var selected = document.querySelectorAll('.selected');
  selected.forEach(card => {
    card.classList.remove('selected');
  });
};

grid.addEventListener('click', event => {

  const clicked = event.target;

  if (
    clicked.nodeName === 'SECTION' ||
    clicked === previousTarget ||
    clicked.parentNode.classList.contains('selected') ||
    clicked.parentNode.classList.contains('match')
  ) {
    return;
  }

  if (count < 2) {
    count++;
    if (count === 1) {
      firstGuess = clicked.parentNode.dataset.name;
      console.log(firstGuess);
      clicked.parentNode.classList.add('selected');
    } else {
      secondGuess = clicked.parentNode.dataset.name;
      console.log(secondGuess);
      clicked.parentNode.classList.add('selected');
    }

    if (firstGuess && secondGuess) {
      if (firstGuess === secondGuess) {
        setTimeout(match, delay);
      }
      setTimeout(resetGuesses, delay);
    }
    previousTarget = clicked;
  }

});